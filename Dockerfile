FROM jetty:9.4-jre8-alpine

# RUN /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
# RUN brew install sbt@1
# RUN sbt package

COPY ./target/scala-2.12/encrypt-decrypt-micro-service_2.12-0.1.0-SNAPSHOT.war $JETTY_BASE/webapps
RUN mv $JETTY_BASE/webapps/encrypt-decrypt-micro-service_2.12-0.1.0-SNAPSHOT.war $JETTY_BASE/webapps/ROOT.war
USER root
RUN apk add --update \
    curl \
    && rm -rf /var/cache/apk/*
EXPOSE 8080

CMD java -jar $JETTY_HOME/start.jar -Djetty.port=8080