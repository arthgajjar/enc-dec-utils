package in.juspay.encDecUtils


import com.upi.merchanttoolkit.security.UPISecurity
import org.scalatra._
import org.slf4j.{Logger, LoggerFactory}

class enc_Dec_Ms extends ScalatraServlet {

  val logger = LoggerFactory.getLogger(getClass)
  val encKey = sys.env("ENC_DEC_KEY")

  get("/"){ }

  post("/decrypt"){
    val value = params("requestMsg")
    logger.info("Decrypted Request : "+value)
    if(value.length() != 0){
      var o = new UPISecurity()
      var dec = o.decrypt(value, encKey)
      dec
    } else {
      ""
    }
  }

  post("/encrypt"){
    val value = params("requestMsg")
    if(value.length() != 0){
      var o = new UPISecurity()
      var enc = o.encrypt(value, encKey)
      logger.info("Encrypted Response : "+enc)
      enc
    } else {
      ""
    }
  }

}
