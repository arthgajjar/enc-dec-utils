package com.example.app

import org.scalatra.test.scalatest._

class enc_Dec_MsTests extends ScalatraFunSuite {

  addServlet(classOf[enc_Dec_Ms], "/*")

  test("GET / on enc_Dec_Ms should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

}
